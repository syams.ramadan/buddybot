package com.bot.buddybot;

public class Stress extends Mood {
    public Stress(){
        quote = new QuotesPantangMenyerah();
        //
        saran = new SaranTempatRefreshing();
        response = new NoResponse();
        foto = new NoFoto();
    }
}
