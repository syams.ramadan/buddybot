package com.bot.buddybot;

public abstract class Mood {
    private IQuotes quote;
    private ISaran saran;
    private IFotoKucing foto;
    private IResponse response;

    public String getQuotes(){
        return quote.getQuotes();
    }

    public String getSaran(){
        return saran.getSaran();
    }

    public String getFotoKucing(){
        return foto.getFotoKucing();
    }

    public String getQuotes(){
        return response.getResponse();
    }

    public setQuote(IQuote quote){
        this.quote = quote;
    }

    public setSaran(ISaran saran){
        this.saran = saran;
    }

    public void setFoto(IFotoKucing foto) {
        this.foto = foto;
    }

    public void setResponse(IResponse response) {
        this.response = response;
    }

}
