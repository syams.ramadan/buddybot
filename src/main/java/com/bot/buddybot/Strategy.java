public interface Strategy {
    String getQuotes();
    String getSaran();
    String getFotoKucing();
    String getResponse();
}
